const element = {
    getId: function (el) {
        return document.getElementById(el);
    },
    getClassName: function (el) {
        return document.getElementsByClassName(el);
    }
}

// get elements
const todoDOM = element.getId('todo')
const newTaskDOM = element.getId('newTask');
const completeTaskDOM = element.getId('completed');
const btnAddItemTask = element.getId('addItem');
const btnSortAToZ = element.getId('two');
const btnSortZToA = element.getId('three');

const renderHTML = (element, data) => {
    return element.innerHTML = data;
}


const taskController = new TaskController(TaskModel);

// remove task
const removeItemTask = (index) => {
    taskController.removeTask(index);
    renderHTML(todoDOM, taskController.renderTaskList());
}

// add to complete zone
const completeTask = (index) => {
    const completedTaskListDOM = taskController.completeTask(index);
    renderHTML(completeTaskDOM, completedTaskListDOM);
    renderHTML(todoDOM, taskController.renderTaskList());
}

const main = () => {

    const taskListDOM = taskController.renderTaskList();
    const taskCompletedListElement = taskController.showCompletedTask();
    renderHTML(todoDOM, taskListDOM);
    renderHTML(completeTaskDOM, taskCompletedListElement);

    // add task
    btnAddItemTask.addEventListener('click', (e) => {
        if (!newTaskDOM.value) return false;
        const newTask = new TaskModel(newTaskDOM.value);
        taskController.addTask(newTask);
        const taskListDOM = taskController.renderTaskList();
        renderHTML(todoDOM, taskListDOM);
        newTaskDOM.value = '';

    });

    // button A to Z
    btnSortAToZ.addEventListener('click', (e) => {
        const taskSortedList = taskController.sort(function (a, b) {
            if (a.nameTask < b.nameTask) {
                return -1;
            }
            if (a.nameTask > b.nameTask) {
                return 1;
            }
            return 0;
        });
        const taskListDOM = taskController.renderTaskList(taskSortedList);
        renderHTML(todoDOM, taskListDOM);
    });

    // button Z to A
    btnSortZToA.addEventListener('click', (e) => {
        const taskSortedList = taskController.sort(function (a, b) {
            if (a.nameTask > b.nameTask) {
                return -1;
            }
            if (a.nameTask < b.nameTask) {
                return 1;
            }
            return 0;
        });
        const taskListDOM = taskController.renderTaskList(taskSortedList);
        renderHTML(todoDOM, taskListDOM);
    })

}

document.addEventListener('DOMContentLoaded', main)