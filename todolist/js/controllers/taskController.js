class TaskController {
    taskList = [];
    taskCompleted = [];
    constructor(taskModel) {
        this.taskModel = taskModel;
    }

    addTask(task) {
        const taskListLocal = getItemLocalStorage("taskList") || this.taskList;
        taskListLocal.push(task);
        addLocalStorage("taskList", taskListLocal);
        return taskListLocal;
    }

    removeTask(index) {
        const changedItemTaskList = getItemLocalStorage("taskList");
        changedItemTaskList.splice(index, 1);
        addLocalStorage("taskList", changedItemTaskList);
        return changedItemTaskList;
    }

    removeCompletedTask(index) {

    }

    showCompletedTask() {
        let contentHTML = "";
        const taskCompletedList = getItemLocalStorage("taskCompletedList") || this.taskCompleted;
        taskCompletedList.map((task, index) => {
            contentHTML += `
            <li class="todo__task" id="todo__task">
            <p>
                ${task?.nameTask}
            </p>
            <i class="fa-regular removeItem fa-trash-can" id="removeItem" ></i>
            <i style="color:green; font-weight: bold" class="fa-regular fa-circle-check" ></i>
        </li>`;
        });
        return contentHTML;
    }

    completeTask(index) {
        const taskCompletedList =
            getItemLocalStorage("taskCompletedList") || this.taskCompleted;
        const taskLocal = getItemLocalStorage("taskList");
        const isExistTask = taskCompletedList.some((taskCompleted) => {
            return taskCompleted?.id === taskLocal[index]?.id;
        });
        if (isExistTask) return this.showCompletedTask();
        taskCompletedList.push(taskLocal[index]);
        addLocalStorage("taskCompletedList", taskCompletedList);
        this.removeTask(index);
        return this.showCompletedTask();
    }

    // sort A to Z, Z to A
    sort(condition) {
        const taskItemList = getItemLocalStorage("taskList") || this.taskList;
        taskItemList.sort(condition);
        return taskItemList;
    }

    renderTaskList(taskListArg) {
        let contentHTML = "";
        const taskItemList = taskListArg || getItemLocalStorage("taskList") || this.taskList;

        taskItemList.map((taskItem, index) => {
            let currentTask = taskItem;
            let contentTask = `
            <li class="todo__task" id="todo__task">
                            <p>
                                ${currentTask.nameTask}
                            </p>
                            <i class="fa-regular removeItem fa-trash-can" id="removeItem" onclick="removeItemTask(${index})"></i>
                            <i class="fa-regular fa-circle-check" onclick="completeTask(${index})"></i>
                        </li>`;
            contentHTML += contentTask;
        });

        return contentHTML;
    }
}
