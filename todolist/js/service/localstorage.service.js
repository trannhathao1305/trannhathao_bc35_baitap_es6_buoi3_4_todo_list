const addLocalStorage = (key, data) => {
    let dataLocalStorage = JSON.stringify(data);
    localStorage.setItem(key, dataLocalStorage);
}

const getItemLocalStorage = (key) => {
    const dataJSON = localStorage.getItem(key);
    return JSON.parse(dataJSON);
    return dataJSON;
}

const removeLocalStorage = (key) => {
    localStorage.removeItem(key);
}